package passwd

import "testing"

func TestCompareCorrectPass(t *testing.T) {
	pass := []byte("abababab")
	p, err := New(pass)
	if err != nil {
		t.Fatal(err)
	}
	if !p.Test(pass) {
		t.Error("Expected Compare to succeed")
	}
}

func TestCompareIncorrectPass(t *testing.T) {
	p, err := New([]byte("abababab"))
	if err != nil {
		t.Fatal(err)
	}
	if p.Test([]byte("abababac")) {
		t.Error("Expected Compare to fail")
	}
}

func TestStringEncoding(t *testing.T) {
	pass := []byte("xyz123")
	p1, err := New(pass)
	if err != nil {
		t.Fatal(err)
	}
	p2 := Passwd(string(p1))
	if !p2.Test(pass) {
		t.Error("Expected Decoded password to match")
	}
}
