package passwd

import (
	"code.google.com/p/go.crypto/bcrypt"
)

const (
	defaultCost = 10
)

type Passwd []byte

// Timing insenstive compare of password.
func (p Passwd) Test(password []byte) bool {
	return bcrypt.CompareHashAndPassword(p, password) == nil
}

// Create a new hashed password.
func New(password []byte) (p Passwd, err error) {
	b, err := bcrypt.GenerateFromPassword(password, defaultCost)
	return Passwd(b), err
}
